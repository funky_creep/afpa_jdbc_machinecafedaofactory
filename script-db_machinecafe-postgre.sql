CREATE DATABASE db_machinecafe;

CREATE TABLE boisson(
    id SERIAL,
    nom varchar(20) not null,
    prix int,
    qte int,
    CONSTRAINT pk_boisson PRIMARY KEY(id)
);

CREATE TABLE piece(
    id SERIAL CONSTRAINT pk_piece PRIMARY KEY,
    nom varchar(20) not null,
    prix int,
    qte int
);

INSERT INTO boisson(nom, prix, qte) VALUES
('Café', 50, 5),
('Thé', 20, 10),
('Chocolat', 50, 5);

INSERT INTO piece VALUES 
(DEFAULT, '10', 10, 5),
(DEFAULT, '20', 20, 5),
(DEFAULT, '50', 50, 5),
(DEFAULT, '100', 100, 5),
(DEFAULT, '200', 200, 5);