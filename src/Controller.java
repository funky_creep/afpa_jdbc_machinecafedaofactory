import exceptions.ExceptionRequest;
import model.DAOFactory;
import model.Stockeur;
import model.beans.Produit;
import model.utils.Connexion;


public class Controller {

	public static void main(String[] args) {

	         // On cr�e la connexion 
	         Connexion connect = new Connexion("jdbc:postgresql://localhost:5432/db_machinecafe", "postgres", "postgres", "org.postgresql.Driver");
	         DAOFactory monUsineADao = new DAOFactory(connect);
	         
	         // Recherche du produit ayant l'id 2
	         try {
				monUsineADao.getDAO(DAOFactory.DAOBoisson).findById(2).getNom();
	         } catch (ExceptionRequest e) {
				//System.out.println(e.getMessage());
	         } finally {
	        	 System.out.println("");
	         }
	         
	         // Recherche de tous les produits
	         try {
				Stockeur<Produit> stockProduits = monUsineADao.getDAO(DAOFactory.DAOBoisson).findAll();
		     } catch (ExceptionRequest e) {
				//System.out.println(e.getMessage());
		     } finally {
		    	 System.out.println("");
		     }
	         
	         // Cr�ation d'un nouveau produit
	        try {
				monUsineADao.getDAO(DAOFactory.DAOBoisson).create(new Produit(12, "Tchango", 20, 5));
			} catch (ExceptionRequest e) {
				//System.out.println(e.getMessage());
			} finally {
				System.out.println("");
			}
	         
	         // Update d'un produit de la table
			try {
				Produit produitTestUpdate = new Produit(2, "Pepsi", 120, 3);
				monUsineADao.getDAO(monUsineADao.DAOBoisson).update(produitTestUpdate);
			} catch (ExceptionRequest e) {
				//System.out.println(e.getMessage());
			} finally {
				System.out.println("");
			}
			
			// Suppression d'un produit de la table avec son id 
			try {
				monUsineADao.getDAO(DAOFactory.DAOBoisson).delete(1);
			} catch (ExceptionRequest e) {
				//System.out.println(e.getMessage());
			} finally {
				System.out.println("");
			}
	         
	         // On ferme la connexion
	         connect.closeConnection();
	}

}
