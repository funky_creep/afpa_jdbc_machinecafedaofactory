package exceptions;

import model.utils.JournalLog;

public class ExceptionRequest extends ExceptionHandler {

	private static final long serialVersionUID = 8373504785051588376L;

	public ExceptionRequest(String msg) {
		super(msg, JournalLog.WARN);
	}
	
}
