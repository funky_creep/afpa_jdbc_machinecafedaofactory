package model;

import model.dao.DAO;
import model.dao.DAOProduit;
import model.utils.Connexion;

public class DAOFactory {
	public static final int DAOBoisson 	= 11;
	public static final int DAOPiece	= 0;
	
	private Connexion cxion = null;
	
	
	public DAOFactory(Connexion cxion) {
		this.cxion = cxion;
	}


	public DAO getDAO(int qui) {
		switch (qui) {
			case DAOBoisson : 
				return new model.dao.DAOProduit(cxion);
			case DAOPiece : 
				return new model.dao.DAOPiece(cxion);
			default :
				return null;
			
		}
	}

}
