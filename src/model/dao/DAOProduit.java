package model.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import exceptions.ExceptionRequest;
import model.Stockeur;
import model.beans.Produit;
import model.utils.Connexion;
import model.utils.JournalLog;

/**
 * <b>Permet de faire les requ�tes sur la table produit</b>
 * <p>
 * Les diff�rents requ�tes du DAOProduit sont les suivantes :
 * <ul>
 * <li>Insert</li>
 * <li>Update</li>
 * <li>Delete</li>
 * <li>Select 1 �l�ment</li>
 * <li>Select *</li>
 * </ul>
 * </p>
 * @author imRoot
 *
 */
public class DAOProduit extends DAO<Produit> {
	
	/**
	 * La requ�te INSERT du DAOProduit
	 */
	private static final String REQINSERT = "INSERT INTO boisson(id, nom, prix, qte) VALUES (?, ?, ?, ?);";
	
	/**
	 * La requ�te UPDATE du DAOProduit
	 */
	private static final String REQUPDATE = "UPDATE boisson SET nom = ?, prix = ?, qte = ? WHERE id = ?;";
	
	/**
	 * La requ�te DELETE du DAOProduit
	 */
	private static final String REQDELETEID = "DELETE FROM boisson WHERE id = ?;";
	
	/**
	 * La requ�te SELECT du DAOProduit
	 */
	private static final String REQSELECT = "SELECT * FROM boisson WHERE id = ?;";
	
	/**
	 * La requ�te SELECT * du DAOProduit
	 */
	private static final String REQSELECTALL = "SELECT * from boisson;";
	
	public DAOProduit(Connexion pCxion) {
		super(pCxion);
	}
	
	/**
	 * Insert un �l�ment dans la table produit
	 * @param objACreer
	 * @return Le produit cr��
	 * @throws ExceptionRequest Si la requ�te comporte une erreur
	 */
	@Override
	public Produit create(Produit objACreer) throws ExceptionRequest {
		try {
			/*PreparedStatement ps = getCxion().getC().prepareStatement(REQINSERT);
			ps.setInt(1, objACreer.getId());
			ps.setString(2, objACreer.getNom());
			ps.setInt(3, objACreer.getValeur());
			ps.setInt(4, objACreer.getQte());
			ps.executeUpdate();
			
			JournalLog.addMessage(JournalLog.DEBUG, "Produit avec l'id " + objACreer.getId() + " et le nom " + objACreer.getNom() + " ajout�");
			return new Produit(objACreer.getId(), objACreer.getNom(), objACreer.getValeur(), objACreer.getQte());*/
			
			PreparedStatement pStmt=getCxion().getC().prepareStatement(REQINSERT);

            pStmt.setInt    (1, objACreer.getId());
            pStmt.setString    (2, objACreer.getNom());
            pStmt.setInt    (3, objACreer.getValeur());
            pStmt.setInt(4, objACreer.getQte());

            System.out.println(objACreer.getId()+" "+objACreer.getNom()+" " +objACreer.getValeur()+ " "+objACreer.getQte());

            //ResultSet rs = pStmt.executeQuery();
            pStmt.executeQuery();

            /*rs.next();
            produit = new Produit(
                    rs.getInt("id_boisson"),
                    rs.getString("name_boisson"),
                    rs.getInt("number_boisson")
                    );/*/

            JournalLog.addMessage(JournalLog.DEBUG, "tout s'est bien execut�"); 
            return new Produit(objACreer.getId(), objACreer.getNom(), objACreer.getValeur());
		}
		catch(SQLException e) {
			throw new ExceptionRequest("Vous ne pouvez pas ins�rer cet �l�ment car il existe d�j� dans la base de donn�es");
		}
	}
	
	/**
	 * Modifie un �l�ment de la table produit
	 * @param objAModifier
	 * @return Le produit mis � jour
	 * @throws ExceptionRequest Si la requ�te comporte une erreur
	 */
	@Override
	public Produit update(Produit objAModifier) throws ExceptionRequest {
		try {
			PreparedStatement ps = getCxion().getC().prepareStatement(REQUPDATE);
			ps.setString(1, objAModifier.getNom());
			ps.setInt(2, objAModifier.getValeur());
			ps.setInt(3, objAModifier.getQte());
			ps.setInt(4, objAModifier.getId());
			
			if (ps.executeUpdate() == 1) {
				JournalLog.addMessage(JournalLog.DEBUG, "Produit avec l'id " + objAModifier.getId() + " et le nom " + objAModifier.getNom() + " modifi�");
				return new Produit(objAModifier.getId(), objAModifier.getNom(), objAModifier.getValeur(), objAModifier.getQte());
			}
			else {
				throw new ExceptionRequest("Vous ne pouvez pas modifier cet �l�ment car il n'existe pas dans la base de donn�es");
			}
		} catch (SQLException e) {
			throw new ExceptionRequest("errr update");
		}
	}
	
	/**
	 * Supprime un �l�ment de la table produit
	 * @param idADeleter
	 * @throws ExceptionRequest Si la requ�te comporte une erreur
	 */
	@Override
	public void delete(int idADeleter) throws ExceptionRequest {
		try {
			PreparedStatement ps = getCxion().getC().prepareStatement(REQDELETEID);
			ps.setInt(1, idADeleter);
			
			if (ps.executeUpdate() == 1) {
				JournalLog.addMessage(JournalLog.DEBUG, "Produit avec l'id " + idADeleter + " supprim�");
			}
			else {
				throw new ExceptionRequest("Vous ne pouvez pas supprimer cet �l�ment car il n'existe pas dans la base de donn�es");
			}
			
		} catch (SQLException e) {
			throw new ExceptionRequest("error suppr");
		}
	}
	
	/**
	 * R�cup�re un �l�ment de la table produit
	 * @param idATrouver
	 * @return le produit r�cup�r�
	 * @throws ExceptionRequest Si la requ�te comporte une erreur
	 */
	@Override
	public Produit findById(int idATrouver) throws ExceptionRequest {
		try {
			PreparedStatement stmt 	= getCxion().getC().prepareStatement(REQSELECT);
			stmt.setInt(1, idATrouver);
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				JournalLog.addMessage(JournalLog.DEBUG, "Produit avec l'id " + idATrouver + " trouv�");
				return new Produit(rs.getInt("id"), rs.getString("nom"), rs.getInt("prix"), rs.getInt("qte"));	
			}
			else {
				throw new ExceptionRequest("Cet �l�ment n'existe pas dans la table");
			}
		} catch (SQLException e) {
			throw new ExceptionRequest("error findbyid");
		}
	}

	/**
	 * R�cup�re tous les �l�ments de la table produit
	 * @return le stockeur compos� de tous les produits de la table
	 * @throws ExceptionRequest Si la requ�te comporte une erreur
	 */
	@Override
	public Stockeur<Produit> findAll() throws ExceptionRequest {
		Stockeur<Produit> stockBoissons = new Stockeur<Produit>();
		try {
			PreparedStatement ps = getCxion().getC().prepareStatement(REQSELECTALL);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				stockBoissons.addItem(new Produit(rs.getInt("id"), rs.getString("nom"), rs.getInt("prix"), rs.getInt("qte")));
			}
			
			JournalLog.addMessage(JournalLog.DEBUG, "Produits trouv�s");
			return stockBoissons;
		} catch (SQLException e) {
			throw new ExceptionRequest("error findAll");
		}
	} 

}
